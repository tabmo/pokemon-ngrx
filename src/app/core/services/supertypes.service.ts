import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SupertypesService {

  constructor(private http: HttpClient) { }

  /**
   * Call API in order to get supertypes
   */
  getSupertypes = (): Observable<{ supertypes: string[]; }> => {
    return this.http.get<{ supertypes: string[]; }>(`${environment.endpoint}supertypes`);
  }
}
