import { TestBed } from '@angular/core/testing';

import { TypesService } from './types.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('TypesService', () => {
  let service: TypesService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ]
    });
    service = TestBed.inject(TypesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
