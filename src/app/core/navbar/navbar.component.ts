import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgbDropdown } from '@ng-bootstrap/ng-bootstrap';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AppState } from 'src/app/store/app.state';
import FilterState from 'src/app/store/filters/filter.state';
import ShippingCartState from 'src/app/store/shipping-cart/shipping-cart.state';
import * as FilterActions from '../../store/filters/filter.actions';
import { SearchFilter } from '../models/search-filter';

@Component({
  selector: 'tabmo-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  @ViewChild(NgbDropdown) private cartSummaryDropdown: NgbDropdown;
  shippingCartState$: Observable<ShippingCartState>;
  filterState$: Observable<FilterState>;
  filters: SearchFilter;

  constructor(
    private router: Router,
    private store: Store<AppState>
  ) {
    this.shippingCartState$ = store.pipe(select('shippingCart'));

    this.filterState$ = store.pipe(select('filter'));
  }

  ngOnInit(): void {
    this.filterState$.subscribe(filterState => {
      this.filters = { ...filterState.filter };
    });
   }

  /**
   * Reset filters and navigate to cards page.
   */
  navigateToCardsPage = () => {
    this.router.navigate(['/cards']).then(() => {
      this.store.dispatch(FilterActions.resetFilter());
    });
  }

  /**
   * Navigate to /cards page and show data according to my filters.
   */
  search = (): void => {
    this.router.navigate(['/cards'], { queryParams: { page: 1 }} ).then(() => {
      this.store.dispatch(FilterActions.setFilter({ filter: this.filters }));
    });
  }

  /**
   * Close cart summary manually.
   */
  closeCartSummary = () => {
    this.cartSummaryDropdown.close();
  }
}
