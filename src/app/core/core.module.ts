import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CartSummaryComponent } from './cart-summary/cart-summary.component';
import { HttpErrorsInterceptor } from './interceptors/http-errors.interceptor';
import { LoaderInterceptor } from './interceptors/loader.interceptor';
import { PaginationInterceptor } from './interceptors/pagination.interceptor';
import { LoaderComponent } from './loader/loader.component';
import { NavbarComponent } from './navbar/navbar.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { QuantityFormComponent } from './quantity-form/quantity-form.component';
import { CardResolver } from './resolvers/card.resolver';

@NgModule({
  declarations: [
    NavbarComponent,
    NotFoundComponent,
    CartSummaryComponent,
    LoaderComponent,
    QuantityFormComponent
  ],
  exports: [
    NavbarComponent,
    LoaderComponent,
    QuantityFormComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    HttpClientModule,
    NgbModule,
    FormsModule
  ],
  providers: [
    CardResolver,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoaderInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorsInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: PaginationInterceptor,
      multi: true
    }
  ]
})
export class CoreModule { }
