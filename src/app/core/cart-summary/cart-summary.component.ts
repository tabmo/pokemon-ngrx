import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AppState } from 'src/app/store/app.state';
import ShippingCartState from 'src/app/store/shipping-cart/shipping-cart.state';
import * as ShippingCartActions from '../../store/shipping-cart/shipping-cart.actions';
import { CardWithQuantity } from '../models/card-with-quantity';

@Component({
  selector: 'tabmo-cart-summary',
  templateUrl: './cart-summary.component.html',
  styleUrls: ['./cart-summary.component.scss']
})
export class CartSummaryComponent implements OnInit {
  @Output() closeCartSummary = new EventEmitter();
  cardsWhitQuantity$: Observable<ShippingCartState>;
  cardsWhitQuantity: CardWithQuantity[];


  constructor(
    private router: Router,
    private store: Store<AppState>
  ) {
    this.cardsWhitQuantity$ = store.pipe(select('shippingCart'));
  }

  ngOnInit(): void {
    this.cardsWhitQuantity$.subscribe((shippingCartState: ShippingCartState) => {
      this.cardsWhitQuantity = shippingCartState.cards;
    });
  }

  /**
   * Increament the quantity of a chosen card in shipping cart.
   * @typeparam CardWithQuantity
   */
  incrementCardQuantity = (cardWithQuantity: CardWithQuantity): void => {
    this.store.dispatch(ShippingCartActions.addCard({ cardWithQuantity }));
  }

  /**
   * Decrement the quantity of a chosen card in shipping cart.
   * @typeparam CardWithQuantity
   */
  decrementCardQuantity = (cardWithQuantity: CardWithQuantity): void => {
    this.store.dispatch(ShippingCartActions.decrementCardQuantity({ cardWithQuantity }));
  }

  /**
   * Check if this is the last item in order to change card style.
   * @typeparam CardWithQuantity
   */
  isLastItem = (card: CardWithQuantity): boolean => {
    const index = this.cardsWhitQuantity.indexOf(card);
    if (index === this.cardsWhitQuantity.length - 1) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Navigate to card detail page and close cart summary menu.
   * @typeparam string
   */
  goToCardDetail = (id: string) => {
    if (id) {
      this.router.navigate([`/cards/${id}`]).then(() => {
        this.closeCartSummary.emit();
      });
    }
  }

  /**
   * Navigate to shipping cart page and close cart summary menu.
   */
  goToShippingCart = () => {
    this.router.navigate(['/my-cart']).then(() => {
      this.closeCartSummary.emit();
    });
  }

}
