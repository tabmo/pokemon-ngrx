import { Card } from './card';

export class CardWithQuantity {
    card: Card;
    quantity: number;
}
