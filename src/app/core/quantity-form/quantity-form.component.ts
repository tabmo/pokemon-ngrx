import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'tabmo-quantity-form',
  templateUrl: './quantity-form.component.html',
  styleUrls: ['./quantity-form.component.scss']
})
export class QuantityFormComponent implements OnInit {
  @Input() quantity = 1;
  @Output() addCard = new EventEmitter<any>();
  isSubmitted: boolean;
  errorMessage: string;

  constructor() {
    this.isSubmitted = false;
  }

  ngOnInit(): void {
  }

  submit = () => {
    this.isSubmitted = true;
    if (this.quantity > 0) {
      this.addCard.emit(this.quantity);
      this.quantity = 1;
      this.errorMessage = null;
    } else {
      this.errorMessage = 'The quantity you entered is not valid';
    }
  }

}
