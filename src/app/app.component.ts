import { Component, OnInit } from '@angular/core';
import { HttpErrorsService } from './core/services/http-errors.service';

@Component({
  selector: 'tabmo-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  isHttpError = false;

  constructor(public httpErrorsService: HttpErrorsService) { }

  ngOnInit(): void {
    this.httpErrorsService.isError$.subscribe((isError: boolean) => {
      this.isHttpError = isError;
    });
  }
}
