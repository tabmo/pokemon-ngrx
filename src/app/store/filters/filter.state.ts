import { SearchFilter } from 'src/app/core/models/search-filter';

export default class FilterState {
    filter: SearchFilter;
}

export const initializeState = (): FilterState => {
    return { filter: new SearchFilter() };
};
