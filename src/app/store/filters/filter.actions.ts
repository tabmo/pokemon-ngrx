import { createAction, props } from '@ngrx/store';
import { SearchFilter } from 'src/app/core/models/search-filter';



export const getFilter = createAction('[Filter component] Get Filter');

export const setFilter = createAction('[Filter component] Set Filter', props<{ filter: SearchFilter}>());

export const resetFilter = createAction('[Filter component] Reset Filter');
