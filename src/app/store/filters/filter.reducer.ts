import { createReducer, on, Action } from '@ngrx/store';
import * as FilterActions from './filter.actions';
import FilterState from './filter.state';
import { initializeState } from '../filters/filter.state';

const initialState = initializeState();

const reducer = createReducer(initialState,
  on(FilterActions.getFilter, state => state),
  on(FilterActions.setFilter, (state: FilterState, { filter }) => {
    return { ...state, filter };
  }),
  on(FilterActions.resetFilter, () => {
    return initialState;
  })
);

export function filterReducer(state: FilterState, action: Action) {
  return reducer(state, action);
}
