import { createAction, props } from '@ngrx/store';
import { SearchFilter } from 'src/app/core/models/search-filter';
import { Card } from 'src/app/core/models/card';



export const getCards = createAction('[Cards-list component] get Cards');

export const beginGetCards = createAction('[Cards-list component] begin Get Cards', props<{ filter: SearchFilter; page: number }>());

export const successGetCards = createAction('[Cards-list component] success Get Cards', props<{ payload: Card[] }>());

export const errorGetCards = createAction('[Cards-list component] - Error', props<Error>());
