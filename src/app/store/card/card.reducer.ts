import { createReducer, on, Action } from '@ngrx/store';
import * as CardActions from './card.actions';
import CardState, { initializeState } from './card.state';

const initialState = initializeState();

const reducer = createReducer(initialState,
  on(CardActions.getCards, state => state),
  on(CardActions.successGetCards, (state: CardState, { payload }) => {
    return { ...state, cards: payload, cardError: null };
  }),
  on(CardActions.errorGetCards, (state: CardState, error: Error) => {
    return { ...state, ToDoError: error };
  })
);

export function cardReducer(state: CardState, action: Action) {
  return reducer(state, action);
}
