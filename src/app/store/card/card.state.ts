import { Card } from 'src/app/core/models/card';

export default class CardState {
    cards: Card[];
    cardError: Error;
}

export const initializeState = (): CardState => {
    return { cards: null, cardError: null };
};
