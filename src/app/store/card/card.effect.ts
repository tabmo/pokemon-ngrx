import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { Action } from '@ngrx/store';
import * as CardActions from './card.actions';
import { map, mergeMap, catchError } from 'rxjs/operators';
import { CardsService } from 'src/app/core/services/cards.service';
import { Card } from 'src/app/core/models/card';


@Injectable({
    providedIn: 'root'
})
export class CardEffects {
    constructor(private cardsService: CardsService, private action$: Actions) { }

    GetCards$: Observable<Action> = createEffect(() =>
        this.action$.pipe(
            ofType(CardActions.beginGetCards),
            mergeMap(action =>
                this.cardsService.getCards(action.filter, action.page).pipe(
                    map((data: Card[]) => {
                        return CardActions.successGetCards({ payload: data });
                    }),
                    catchError((error: Error) => {
                        return of(CardActions.errorGetCards(error));
                    })
                )
            )
        )
    );
}