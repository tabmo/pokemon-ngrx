import { createReducer, on, Action } from '@ngrx/store';
import * as ShippingCartActions from './shipping-cart.actions';
import ShippingCartState, { initializeState } from './shipping-cart.state';
import { CardWithQuantity } from 'src/app/core/models/card-with-quantity';

const initialState = initializeState();

const reducer = createReducer(initialState,
    on(ShippingCartActions.getShippingCart, state => state),

    on(ShippingCartActions.addCard, (state: ShippingCartState, { cardWithQuantity }) => {
        const cardToUpdate = state.cards.find((c: CardWithQuantity) => {
            return c.card.id === cardWithQuantity.card.id;
        });

        if (cardToUpdate) {
            const index = state.cards.indexOf(cardToUpdate);
            const cards = JSON.parse(JSON.stringify(state.cards));
            ++cards[index].quantity;
            const { totalAmount, count } = getCountAndTotalAmount(cards);
            return { ...state, cards, count, totalAmount };
        } else {
            state = { ...state, cards: [...state.cards, cardWithQuantity] };
            const { totalAmount, count } = getCountAndTotalAmount(state.cards);
            return { ...state, count, totalAmount };
        }
    }),

    on(ShippingCartActions.addCardWithQuantity, (state: ShippingCartState, { cardWithQuantity }) => {
        const cardToUpdate = state.cards.find((c: CardWithQuantity) => {
            return c.card.id === cardWithQuantity.card.id;
        });

        if (cardToUpdate) {
            const index = state.cards.indexOf(cardToUpdate);
            const cards = JSON.parse(JSON.stringify(state.cards));
            cards[index].quantity += cardWithQuantity.quantity;
            const { totalAmount, count } = getCountAndTotalAmount(cards);
            return { ...state, cards, count, totalAmount };
        } else {
            state = { ...state, cards: [...state.cards, cardWithQuantity] };
            const { totalAmount, count } = getCountAndTotalAmount(state.cards);
            return { ...state, count, totalAmount };
        }
    }),

    on(ShippingCartActions.decrementCardQuantity, (state: ShippingCartState, { cardWithQuantity }) => {
        const index = state.cards.indexOf(cardWithQuantity);
        if (index >= 0) {
            const cards = JSON.parse(JSON.stringify(state.cards));
            if (cardWithQuantity.quantity > 1) {
                --cards[index].quantity;
                const { totalAmount, count } = getCountAndTotalAmount(cards);
                return { ...state, cards, count, totalAmount };
            } else {
                cards.splice(index, 1);
                const { totalAmount, count } = getCountAndTotalAmount(cards);
                return { ...state, cards, count, totalAmount };
            }
        }
        return state;
    }),

    on(ShippingCartActions.deleteCard, (state: ShippingCartState, { cardWithQuantity }) => {
        const index = state.cards.indexOf(cardWithQuantity);
        if (index >= 0) {
            const cards = JSON.parse(JSON.stringify(state.cards));
            cards.splice(index, 1);
            const { totalAmount, count } = getCountAndTotalAmount(cards);
            return { ...state, cards, count, totalAmount };
        }
        return state;
    }),

    on(ShippingCartActions.cleanShippingCart, () => {
        return initialState;
    })
);

export function shippingCartReducer(state: ShippingCartState, action: Action) {
    return reducer(state, action);
}

const getCountAndTotalAmount = (cards: CardWithQuantity[]): { count: number, totalAmount: number } => {
    let count = 0;
    let totalAmount = 0;
    cards.forEach((card: CardWithQuantity) => {
        count += card.quantity;
        totalAmount += card.quantity * card.card.price;
    });
    totalAmount = Math.round(totalAmount * 100) / 100;
    return { count, totalAmount };
};
