import { CardWithQuantity } from '../../core/models/card-with-quantity';

export default class ShippingCartState {
    cards: CardWithQuantity[];
    totalAmount: number;
    count: number;
}

export const initializeState = (): ShippingCartState => {
    return { cards: [], totalAmount: 0, count: 0 };
};
