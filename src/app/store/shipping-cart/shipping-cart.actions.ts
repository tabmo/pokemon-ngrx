import { createAction, props } from '@ngrx/store';
import { Card } from 'src/app/core/models/card';
import { CardWithQuantity } from 'src/app/core/models/card-with-quantity';



export const getShippingCart = createAction('[Shipping cart component] Get Shipping Cart');

export const addCard = createAction('[Shipping cart component] Add Card', props<{ cardWithQuantity: CardWithQuantity }>());

export const addCardWithQuantity = createAction(
    '[Shipping cart component] Add Card With Quantity', props<{ cardWithQuantity: CardWithQuantity }>());

// export const incrementCardQuantity = createAction(
//     '[Shipping cart component] Increment card quantity', props<{ cardWithQuantity: CardWithQuantity }>());

export const decrementCardQuantity = createAction(
    '[Shipping cart component] Decrement Card Quantity', props<{ cardWithQuantity: CardWithQuantity }>());

export const deleteCard = createAction('[Shipping cart component] Delete Card', props<{ cardWithQuantity: CardWithQuantity }>());

export const cleanShippingCart = createAction('[Shipping cart component] Clean Shipping Cart');
