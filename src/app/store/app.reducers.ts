import { ActionReducerMap } from '@ngrx/store';
import { AppState } from './app.state';
import { cardReducer } from './card/card.reducer';
import { filterReducer } from './filters/filter.reducer';
import { shippingCartReducer } from './shipping-cart/shipping-cart.reducer';

export const reducers: ActionReducerMap<AppState> = {
    card: cardReducer,
    filter: filterReducer,
    shippingCart: shippingCartReducer
};
