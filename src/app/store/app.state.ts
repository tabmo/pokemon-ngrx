import CardState from './card/card.state';
import FilterState from './filters/filter.state';
import ShippingCartState from './shipping-cart/shipping-cart.state';

export interface AppState {
    card: CardState;
    filter: FilterState;
    shippingCart: ShippingCartState;
}
