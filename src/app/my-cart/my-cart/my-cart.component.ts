import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { CardWithQuantity } from 'src/app/core/models/card-with-quantity';
import { AppState } from 'src/app/store/app.state';
import ShippingCartState from 'src/app/store/shipping-cart/shipping-cart.state';
import * as ShippingCartActions from '../../store/shipping-cart/shipping-cart.actions';

@Component({
  selector: 'tabmo-my-cart',
  templateUrl: './my-cart.component.html',
  styleUrls: ['./my-cart.component.scss']
})
export class MyCartComponent implements OnInit {
  shippingCartState$: Observable<ShippingCartState>;
  count: number;
  totalAmount: number;
  cardsWhitQuantity: CardWithQuantity[];

  constructor(
    private store: Store<AppState>
  ) {
    this.shippingCartState$ = store.pipe(select('shippingCart'));
  }

  /**
   * Initialize cardsWhitQuantity, count and totalAmount.
   */
  ngOnInit(): void {
    this.shippingCartState$.subscribe((shippingCartState: ShippingCartState) => {
      this.cardsWhitQuantity = shippingCartState.cards;
      this.count = shippingCartState.count;
      this.totalAmount = shippingCartState.totalAmount;
    });
  }

  /**
   * Increase the quantity of a chosen card in my cart.
   * @typeparam CardWithQuantity
   */
  increaseQuantity = (cardWithQuantity: CardWithQuantity): void => {
    this.store.dispatch(ShippingCartActions.addCard({ cardWithQuantity }));
  }

  /**
   * Remove or decrease the quantity of a chosen card in my cart.
   * @typeparam CardWithQuantity
   */
  decreaseOrDelete = (cardWithQuantity: CardWithQuantity): void => {
    this.store.dispatch(ShippingCartActions.decrementCardQuantity({ cardWithQuantity }));
  }

  /**
   * Delete card from my cart.
   * @typeparam CardWithQuantity
   */
  deleteCard = (cardWithQuantity: CardWithQuantity): void => {
    this.store.dispatch(ShippingCartActions.deleteCard({ cardWithQuantity }));
  }

}
