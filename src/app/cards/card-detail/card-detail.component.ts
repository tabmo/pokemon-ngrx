import { Component, OnInit } from '@angular/core';
import { Card } from 'src/app/core/models/card';
import { ActivatedRoute } from '@angular/router';
import * as ShippingCartActions from '../../store/shipping-cart/shipping-cart.actions';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/store/app.state';
import { CardWithQuantity } from 'src/app/core/models/card-with-quantity';

@Component({
  selector: 'tabmo-card-detail',
  templateUrl: './card-detail.component.html',
  styleUrls: ['./card-detail.component.scss']
})
export class CardDetailComponent implements OnInit {
  card: Card;

  constructor(
    private activatedRoute: ActivatedRoute,
    private store: Store<AppState>
  ) { }

  /**
   * Get card data from activatedRoute because we are using a resolver.
   */
  ngOnInit(): void {
    this.activatedRoute.data
      .subscribe((data: { card: Card }) => {
        this.card = data.card;
      });
  }


  /**
   * Add current card with a specific quantity to shipping cart.
   */
  addCard = (quantity: number) => {
    const cardWithQuantity: CardWithQuantity = {
      card: this.card,
      quantity,
    };
    this.store.dispatch(ShippingCartActions.addCardWithQuantity({ cardWithQuantity }));
  }
}
