import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { SearchFilter } from 'src/app/core/models/search-filter';
import { PaginationService } from 'src/app/core/services/pagination.service';
import { AppState } from 'src/app/store/app.state';
import CardState from 'src/app/store/card/card.state';
import FilterState from 'src/app/store/filters/filter.state';
import * as CardActions from '../../store/card/card.actions';

@Component({
  selector: 'tabmo-cards-list',
  templateUrl: './cards-list.component.html',
  styleUrls: ['./cards-list.component.scss']
})
export class CardsListComponent implements OnInit {
  filter: SearchFilter;
  filter$: Observable<FilterState>;
  page: number;
  totalCount: number;
  cards$: Observable<CardState>;

  constructor(
    private paginationService: PaginationService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private store: Store<AppState>
  ) {
    /**
     * By default page = 1.
     */
    this.page = 1;

    this.initTotalCount();

    this.cards$ = store.pipe(select('card'));
    this.filter$ = store.pipe(select('filter'));
  }

  /**
   * Subscribe to filter$ in order to get filter in real time.
   * Call setPage and getCards.
   */
  ngOnInit(): void {
    this.filter$.subscribe((filterState: FilterState) => {
      this.filter = filterState.filter;
      this.setPage();
      this.getCards();
    });
  }

  /**
   * Initialize the total number of elements (across all pages).
   */
  initTotalCount = () => {
    this.paginationService.totalCount.subscribe((totalCount: number) => {
      this.totalCount = totalCount;
    });
  }

  /**
   * Update page data and params when navigating to another page.
   * Call getCards
   */
  updatePage = (): void => {
    this.router.navigate([], { queryParams: { page: this.page } }).then(() => {
      window.scroll(0, 0);
      this.getCards();
    });
  }

  /**
   * Set page by page queryParams value.
   */
  private setPage = (): void => {
    this.activatedRoute.queryParams.subscribe(params => {
      if (params.page) {
        this.page = +params.page;
      }
    });
  }

  /**
   * Dispatch action in order to get cards by filter and page.
   */
  private getCards = (): void => {
    this.store.dispatch(CardActions.beginGetCards({ filter: this.filter, page: this.page}));
  }

  /**
   * Check if the page should display pagination.
   */
  isDisplayPagination = () => {
    if (this.cards$ && this.totalCount > this.filter.pageSize) {
      return true;
    } else {
      return false;
    }
  }
}
