import { Component, Input, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Card } from 'src/app/core/models/card';
import { AppState } from 'src/app/store/app.state';
import * as ShippingCartActions from '../../../store/shipping-cart/shipping-cart.actions';

@Component({
  selector: 'tabmo-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  @Input() card: Card;

  constructor(
    private store: Store<AppState>) { }

  ngOnInit(): void {
  }

  /**
   * Add a card to shipping cart.
   */
  addCard = (card: Card) => {
    const cardWithQuantity = {
      card,
      quantity: 1
    };
    this.store.dispatch(ShippingCartActions.addCard({ cardWithQuantity }));
  }
}
