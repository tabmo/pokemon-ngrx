import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { AppState } from 'src/app/store/app.state';
import { SearchFilter } from 'src/app/core/models/search-filter';
import { SupertypesService } from 'src/app/core/services/supertypes.service';
import { TypesService } from 'src/app/core/services/types.service';
import * as FilterActions from '../../../store/filters/filter.actions';
import { Observable } from 'rxjs';
import FilterState from 'src/app/store/filters/filter.state';

@Component({
  selector: 'tabmo-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnInit {
  filter$: Observable<FilterState>;
  filter: SearchFilter;
  types: string[];
  supertypes: string[];

  /**
   * Subscribe to filter store in order to get filters in real time.
   */
  constructor(
    private typesService: TypesService,
    private supertypesService: SupertypesService,
    private router: Router,
    private store: Store<AppState>
  ) {
    this.filter$ = store.pipe(select('filter'));
  }

  /**
   * Subscribe to filter$ store in order to get filter in real time.
   * Initialize types and supertypes.
   */
  ngOnInit(): void {
    this.filter$.subscribe(filterState => {
      this.filter = { ...filterState.filter };
    });
    this.initTypes();
    this.initSupertypes();
  }

  /**
   * After every change in filterForm reset page to 1 and update filters.
   */
  search = (): void => {
    this.router.navigate(['/cards'], { queryParams: { page: 1 } }).then(() => {
      this.store.dispatch(FilterActions.setFilter({ filter: this.filter }));
    });
  }

  /**
   * Call getTypes from typesService in order to initialize types.
   */
  private initTypes = (): void => {
    this.typesService.getTypes().subscribe((data: { types: string[]; }) => {
      this.types = data.types;
    });
  }

  /**
   * Call getSupertypes from supertypesService in order to initialize supertypes.
   */
  private initSupertypes = (): void => {
    this.supertypesService.getSupertypes().subscribe((data: { supertypes: string[]; }) => {
      this.supertypes = data.supertypes;
    });
  }
}
