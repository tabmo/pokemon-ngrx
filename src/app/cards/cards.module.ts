import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CardsRoutingModule } from './cards-routing.module';
import { CardDetailComponent } from './card-detail/card-detail.component';
import { CardsListComponent } from './cards-list/cards-list.component';
import { CardComponent } from './cards-list/card/card.component';
import { FilterComponent } from './cards-list/filter/filter.component';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CoreModule } from '../core/core.module';


@NgModule({
  declarations: [CardDetailComponent, CardsListComponent, CardComponent, FilterComponent],
  imports: [
    CommonModule,
    CardsRoutingModule,
    FormsModule,
    NgbModule,
    CoreModule
  ]
})
export class CardsModule { }
